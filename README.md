![java programming banner](https://user-images.githubusercontent.com/2780145/34820295-16f0a172-f6e6-11e7-880b-729faf8c6613.png)
![why java banner](https://user-images.githubusercontent.com/2780145/34820316-1f274436-f6e6-11e7-8c5f-269b08b3c08c.png)

<img src = "https://user-images.githubusercontent.com/2780145/34364478-7f0ef056-eaac-11e7-912a-c03fc4ab4743.png" align = "left" width = "50">

# Learn Java Programming

**This Repo contains Java Programs related to Concepts of :**
- *Java Fundamentals*
- *Java Object-Oriented Programming*
- *Java Collections Framework*
- *Design Patterns in Java*
- *Multithreading in Java*
- *Unit Testing in Java*
- *Data Structures in Java*
- *Algorithms in Java*

![quote banner](https://user-images.githubusercontent.com/2780145/34341843-fa43f72c-e9c5-11e7-88e8-f3b863448262.png)
## [1] Java - Fundamentals & OOPS [Done]
### NOTES [Useful Info] :

*[-> All 50 Java Keywords with definitions & examples](_moreReadMe/keywords)*

*[-> Java Features, Execution Process & JVM Internals](_moreReadMe/howItWorks)*

*[-> Java Fundamentals & OOPS Concepts](java_basicsI_and_oops)*

*[-> Java OOPS - few points to remember](_moreReadMe/oopsRules)*

*[-> Methods of Number, Char, String, Array Classes](_moreReadMe/importantMethods)*

### PROGRAMS :
#### [Variables & Operators](java_basicsI_and_oops/variables_and_operators) | [Control Flow](java_basicsI_and_oops/control_flow) 

#### [Classes & Objects](java_basicsI_and_oops/class_and_object) | [static & this](java_basicsI_and_oops/static_and_this) | [Inner Classes](java_basicsI_and_oops/inner_class)

#### [Inheritance (is-a)](java_basicsI_and_oops/inheritance) | [Association (has-a, part-of)](java_basicsI_and_oops/association)

#### [Overloading](java_basicsI_and_oops/method_overloading) | [Overriding](java_basicsI_and_oops/method_overriding) | [super & final](java_basicsI_and_oops/super_and_final)

#### [Runtime Polymorphism](java_basicsI_and_oops/runtime_polymorphism) | [instanceof](java_basicsI_and_oops/instanceof)

#### [Abstraction](java_basicsI_and_oops/abstraction) | [Interfaces](java_basicsI_and_oops/interfaces) | [Encapsulation](java_basicsI_and_oops/encapsulation)

#### [Arrays](java_basicsI_and_oops/arrays) | [Strings](java_basicsI_and_oops/strings) | [Date & Time](java_basicsI_and_oops/date_time) | [Misc](java_basicsI_and_oops/miscellaneous)

.

## [2] Java - Generics, Collections, Exceptions, I/O [Done]
### NOTES [Useful Info] :
*[-> Java Generics](_moreReadMe/generics)*

*[-> Java Collections Framework](java_basicsII_and_collections)*

*[-> Java Errors and Exception Handling](_moreReadMe/exceptions)*

*[-> Java Files and I/O](_moreReadMe/input_output)*

*[-> Java Serialization](_moreReadMe/serialization)*

### PROGRAMS :
#### [Generics in Java](java_basicsII_and_collections/generics)

#### [Collection Interface (List,Set,Queue)](java_basicsII_and_collections/collection_interface)

#### [Map Interface (Map)](java_basicsII_and_collections/map_interface)

#### [Legacy (Enum,Vector,Stack,etc)](java_basicsII_and_collections/legacy_ds)

#### [Collections Class](java_basicsII_and_collections/collections_class)

#### [Exception Handling](java_basicsII_and_collections/exceptions)

#### [Files and I/O](java_basicsII_and_collections/input_output)

#### [Serialization](java_basicsII_and_collections/serialization)

.

## [3] Software Design [WIP]

### NOTES [Useful Info] :
*[-> Introduction to Design Patterns](_moreReadMe/design_patterns_intro)*

*[-> Creational Design Patterns](java_design_patterns/gof_creational)*

*[-> Structural Design Patterns](java_design_patterns/gof_structural)*

*[-> Behavioral Design Patterns](java_design_patterns/gof_behavioral)*

*[-> Design Patterns Cheat Sheet](_moreReadMe/design_patterns_cheatsheet)*

### PROGRAMS :

#### [GOF Design Patterns - Creational](java_design_patterns/gof_creational)

#### [GOF Design Patterns - Structural](java_design_patterns/gof_structural)

#### [GOF Design Patterns - Behavioral](java_design_patterns/gof_behavioral)

#### SOLID Design Principles 

#### Design Interview Questions

.

## [4] Java - Concurrency & Other Topics [WIP]
### NOTES [Useful Info] :
*[-> Java Multithreading & Garbage Collection](_moreReadMe/multithreading)*

*[-> Java Thread Synchronization](_moreReadMe/threadSynchronization)*

*[-> Java Testing Basics](_moreReadMe/testing)*

*[-> Java Class Design Basic](_moreReadMe/class_relations)*

*[-> Java RegEx & Lambda Expressions](_moreReadMe/regex_and_lambda)*

### PROGRAMS :
#### [Multithreading](java_concurrency/multithreading)

#### [Thread Synchronization](java_concurrency/synchronization)

#### [Regular Expressions](java_misc_advanced_concepts/regex)

#### [Lambda Expressions](java_misc_advanced_concepts/lambda)

#### BigDecimal & BigIntegar

#### JSON & XML Parsing

#### Unit Testing - JUnit 4 (Library)

#### Class Relationships (Design Basics)

.

## [5] Data Structures & Algorithms [WIP]
### NOTES [Useful Info] :
*[-> Common Data Structures](_moreReadMe/ds)*

*[-> Searching and Sorting Algorithms](_moreReadMe/search_sort)*

*[-> Tree and Graph Algorithms](_moreReadMe/tree_graph)*

*[-> Concepts of Dynamic Programming](_moreReadMe/dp)*

*[-> Big O Cheat Sheet](_moreReadMe/bigO)*

### PROGRAMS :
#### [Basic Programs](java_datastructures_algorithms/basic_programs) | [Recursion Programs](java_datastructures_algorithms/recursion_programs)

#### [Linked List DS](java_datastructures_algorithms/linked_lists) | [BST & Heap DS](java_datastructures_algorithms/bst_heaps)

#### [Searching](java_datastructures_algorithms/searching) | [Sorting](java_datastructures_algorithms/sorting)

#### [Tree Traversal](java_datastructures_algorithms/tree_traversal) | [Graph Traversal](java_datastructures_algorithms/graph_traversal)

#### [Graph Algorithms](java_datastructures_algorithms/graph_classic_algos)

#### [Dynamic Programming](java_datastructures_algorithms/dynamic_programming)

### EXTERNAL LINKS :
**More DS/Algo Programs -** My Repository for Coding Questions (To Do)

.

## Relation of Java and Others Languages
### NOTES [Useful Info] :
*[-> Comparisons between C, C++, C# and Java Language](_moreReadMe/cplusplus)*

*[-> CheatSheet | C# for Java Developers](_moreReadMe/csharp)*

*[-> Java and Python](_moreReadMe/python)*

## Learn Android App Development
### EXTERNAL LINKS :
[My Repository for Learning Android App Development Concepts](https://github.com/Suryakant-Bharti/some-android-information)

.

## Extras
### NOTES :
*[[USEFUL INFO - Good Java & Computer Science Books for Reference]](_moreReadMe/books)*

*[[USEFUL INFO - Great Websites for Learning Java & Computer Science]](_moreReadMe/websites)*

.

**NOTE :**

    I will keep adding more important codes to this Repo throughout 2019. 
Happy Learning! :smiley:

.

**REFERENCES :**

    This repository contains some codes and images from other authors.
    Used for "Nonprofit Educational Purpose".
    References are listed in the above link for Books & Websites.
Thanks to those authors! :thumbsup:
